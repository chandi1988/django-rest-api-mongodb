# Django REST API with MongoDB

## Description
A very basic REST API using Django Rest Framework and MongoDB that can create, retrieve, update, delete and find Tutorials by title or published status from the tutorial: https://bezkoder.com/django-mongodb-crud-rest-framework/


## Screenshots

Database Collection in MongoDB Compass:

![database collection](screenshots/mongo_compass.png)

---


POST a new tutorial:

![post new tutorial](screenshots/post_new_tutorial.png)

---


GET all published tutorials:

![get all published](screenshots/get_published_tutorials.png)

---


GET all tutorials with title containing "tut":

![get with title param](screenshots/get_tutorials_by_title.png)

---


GET single tutorial by id:

![get single tutorial by id](screenshots/get_tutorial_id.png)

---


Update single tutorial by id:

![update tutorial](screenshots/put_tutorial_id.png)

---

## Licence

This project is under MIT Licence.
